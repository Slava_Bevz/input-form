const [...inputs] = document.querySelectorAll('input')
inputs.forEach(function (element) {
    element.addEventListener('change', function (e) {
        validate(e.target);
    })
})

function validate(e) {
    let flag = false; 
    if (e.type === 'email') {
        if (!/^[A-z0-9._]+@[a-z0-9._]+.[a-z]{1,4}$/.test(e.value)) {
            flag = true;
        } 
    } else if (e.type === 'tel') {
        if (!/^\+38\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/.test(e.value)) {
            flag = true;
        }
    } else if (e.dataset.userName === 'userName') {
        if (!/^[А-яіІїЇєЄ]+$/.test(e.value)) {
            flag = true;
        }
    } else if (e.dataset.lastName === 'lastName') {
        if (!/^[А-яіІїЇєЄ]+$/.test(e.value)) {
            flag = true;
        }
    } else if (e.placeholder === 'Страна') {
        if (!/^[А-яіІїЇєЄA-z]+$/.test(e.value)) {
            flag = true;
        } 
    } else if (e.placeholder === 'Город') {
        if (!/^[А-яіІїЇєЄA-z]+$/.test(e.value)) {
            flag = true;
        } 
    } else if (e.placeholder === 'Индекс') {
        if (!/^[0-9]+$/.test(e.value)) {
            flag = true;
        } 
    } else {
        if (e.value === '') {
            flag = true;
        }
    }
    if(flag) {
        e.style.border = '3px solid red';
    } else {
        e.style.border = '3px solid rgb(182,182,182)';
    }
}
